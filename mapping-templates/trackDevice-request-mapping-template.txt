{
  "version": "2017-02-28",
  "operation": "PutItem",
  "key": {
    "userId": { "S": "${context.arguments.userId}" },
    "deviceId": { "S": "${context.arguments.device.deviceId}" }
  },
  #set($attrs = {})
  #set($attrs.userId = $util.dynamodb.toDynamoDB($context.arguments.userId))
  #set($attrs.enabled = $util.dynamodb.toDynamoDB($context.arguments.enabled))
  #foreach($entry in $context.arguments.device.entrySet())
    $util.qr($attrs.put($entry.key, $util.dynamodb.toDynamoDB($entry.value)))
  #end
  "attributeValues": $util.toJson($attrs)
}