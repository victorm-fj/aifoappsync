import AWS from 'aws-sdk';

const cognitoIdentityService = new AWS.CognitoIdentityServiceProvider({
	region: 'us-east-1',
});

export const handler = (event, context, callback) => {
	console.log('Received event {}', JSON.stringify(event, null, 2));
	const params = {
		UserPoolId: process.env.COGNITO_POOL_ID,
		Filter: `phone_number = \"${event.request.userAttributes.phone_number}\"`,
		Limit: 1,
	};
	cognitoIdentityService.listUsers(params, (err, data) => {
		if (!err) {
			console.log('listUsers data', data);
			if (
				data.Users &&
				data.Users.length > 0 &&
				data.Users[0].UserStatus === 'CONFIRMED'
			) {
				const error = new Error(
					'This phone number is already associated with another user account'
				);
				console.log('checkPhone error', error);
				callback(error, event);
			} else {
				callback(null, event);
			}
		} else {
			const error = new Error(
				`There was en error in the Cognito pre sign up lambda trigger: ${JSON.stringify(
					err
				)}`
			);
			callback(error, event);
		}
	});
};
