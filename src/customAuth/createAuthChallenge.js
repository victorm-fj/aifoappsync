import { sendCode } from '../services/sns';

export const handler = async (event, context, callback) => {
	console.log('Received event {}', JSON.stringify(event, undefined, 2));
	if (
		event.request.session.length === 0 &&
		event.request.challengeName === 'CUSTOM_CHALLENGE'
	) {
		// Create random 6-digit code
		const answer = Math.random()
			.toString(10)
			.substr(2, 6);
		const message = `Your login code is ${answer}`;
		const phoneNumber = event.request.userAttributes.phone_number;
		try {
			await sendCode(message, phoneNumber);
			console.log('SMS sent');
		} catch (error) {
			console.log('SNS error', JSON.stringify(error, undefined, 2));
		}
		// Set the return parameters - Including the correct answer
		event.response.publicChallengeParameters = {};
		event.response.privateChallengeParameters = {};
		event.response.privateChallengeParameters.answer = answer;
		event.response.challengeMetadata = 'PASSWORDLESS_CHALLENGE';
	}
	callback(null, event);
};
