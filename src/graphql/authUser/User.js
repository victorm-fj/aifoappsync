class User {
	constructor(
		saveUser,
		saveDevice,
		signUp,
		queryUser,
		queryDevices,
		queryInvitee,
		triggerUpdateInvitee
	) {
		this.saveUser = saveUser;
		this.saveDevice = saveDevice;
		this.signUp = signUp;
		this.queryUser = queryUser;
		this.queryDevices = queryDevices;
		this.queryInvitee = queryInvitee;
		this.triggerUpdateInvitee = triggerUpdateInvitee;
	}
	async register(args, callback) {
		const { username, password, phoneNumber, firstName, device } = args;
		const user = {
			userId: username,
			phoneNumber,
			firstName,
			status: 'registered',
		};
		try {
			const params = {
				ClientId: process.env.COGNITO_APP_CLIENT_ID,
				Password: password,
				Username: username,
				UserAttributes: [
					{ Name: 'phone_number', Value: phoneNumber },
					{ Name: 'given_name', Value: firstName },
				],
			};
			await this.signUp(params);
			await this.saveUser({ ...user, password });
			await this.saveDevice({
				userId: username,
				...device,
				phoneNumber,
				enabled: true,
			});
			console.log('User registered');
			const data = await this.queryInvitee(phoneNumber);
			if (data.Count > 0) {
				// New user has been invited
				const { userId, invitee } = data.Items[0];
				console.log('invitee', data.Items[0]);
				// Save invitee with new status
				const status = 'created';
				// Trigger updateInvitee mutation so that the client app
				// gets notified of invitee status changes through subscription
				this.triggerUpdateInvitee(userId, invitee, status);
			}
			callback(null, user);
		} catch (error) {
			console.log('Register user error', error);
			callback(error, null);
		}
	}
	async getDevices(phoneNumber, callback) {
		try {
			const data = await this.queryUser(phoneNumber);
			if (data.Count > 0) {
				const { userId } = data.Items[0];
				const devicesData = await this.queryDevices(userId);
				callback(null, devicesData.Items);
			} else {
				callback(null, []);
			}
		} catch (error) {
			console.log('Get devices error', error);
			callback(error, null);
		}
	}
}

export default User;
