import {
	saveUser,
	saveDevice,
	queryUser,
	queryDevices,
	queryInvitee,
} from '../../services/dynamodb';
import { signUp } from '../../services/cognito';
import { triggerUpdateInvitee } from '../../services/appsync';
import User from './User';

const user = new User(
	saveUser,
	saveDevice,
	signUp,
	queryUser,
	queryDevices,
	queryInvitee,
	triggerUpdateInvitee
);

export default (event, callback) => {
	console.log('authUser invoked');
	if (event.field === 'registerUser') {
		const {
			arguments: { username, password, firstName, phoneNumber, device },
		} = event;
		user.register(
			{ username, password, firstName, phoneNumber, device },
			callback
		);
	} else {
		const {
			arguments: { phoneNumber },
		} = event;
		user.getDevices(phoneNumber, callback);
	}
};
