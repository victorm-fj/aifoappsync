import authUser from './authUser';
import userAccounts from './userAccounts';

export const handler = (event, context, callback) => {
	console.log('Received event {}', JSON.stringify(event, undefined, 2));
	switch (event.field) {
	case 'registerUser':
	case 'getDevicesByPhone':
		authUser(event, callback);
		break;
	case 'addAccount':
		userAccounts(event, callback);
		break;
	default:
		callback(`Unknown field, unable to resolve ${event.field}`, null);
		break;
	}
};
