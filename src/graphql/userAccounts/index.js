import { saveAccount } from '../../services/dynamodb';
import { getAccessToken } from '../../services/plaid';
import Account from './Account';

const account = new Account(saveAccount, getAccessToken);

export default (event, callback) => {
	console.log('userAccounts invoked');
	const {
		arguments: { userId, publicToken, institution, accounts },
	} = event;
	account.save({ userId, publicToken, institution, accounts }, callback);
};
