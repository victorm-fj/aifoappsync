class Account {
	constructor(saveAccount, getAccessToken) {
		this.saveAccount = saveAccount;
		this.getAccessToken = getAccessToken;
	}
	async save(args, callback) {
		const { userId, publicToken, institution, accounts } = args;
		try {
			const token = await this.getAccessToken(publicToken);
			const accountItem = {
				userId,
				accessToken: token.access_token,
				itemId: token.item_id,
				institution,
				accounts,
			};
			await this.saveAccount(accountItem);
			callback(null, accountItem);
		} catch (error) {
			console.log('Save account error', error);
			callback(error, null);
		}
	}
}

export default Account;
