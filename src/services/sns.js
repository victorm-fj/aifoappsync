import AWS from 'aws-sdk';

AWS.config.update({ region: 'us-east-1' });

export const call = (action, params) => {
	const sns = new AWS.SNS();
	return sns[action](params).promise();
};

export const sendCode = (message, phoneNumber) => {
	const params = {
		Message: message,
		MessageAttributes: {
			'AWS.SNS.SMS.SenderID': {
				DataType: 'String',
				StringValue: 'AIFO',
			},
			'AWS.SNS.SMS.SMSType': {
				DataType: 'String',
				StringValue: 'Transactional',
			},
		},
		PhoneNumber: phoneNumber,
	};
	return call('publish', params);
};
