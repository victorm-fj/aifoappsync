import AWS from 'aws-sdk';
import axios from 'axios';

export const triggerUpdateInvitee = (userId, invitee, status) => {
	let req = new AWS.HttpRequest(
		'https://uwrv7q2rd5ecfff2hi45hcjnga.appsync-api.us-east-1.amazonaws.com/graphql',
		'us-east-1'
	);
	req.method = 'POST';
	req.headers.host =
		'uwrv7q2rd5ecfff2hi45hcjnga.appsync-api.us-east-1.amazonaws.com';
	req.headers['Content-Type'] = 'multipart/form-data';
	req.body = JSON.stringify({
		query:
			'mutation UpdateInvitee($userId: ID!, $invitee: String!, $status: String!) { updateInvitee(userId: $userId, invitee: $invitee, status: $status) { __typename userId invitee name status } }',
		variables: { userId, invitee, status },
	});
	let signer = new AWS.Signers.V4(req, 'appsync', true);
	signer.addAuthorization(AWS.config.credentials, AWS.util.date.getDate());
	return axios({
		method: 'post',
		url:
			'https://uwrv7q2rd5ecfff2hi45hcjnga.appsync-api.us-east-1.amazonaws.com/graphql',
		data: req.body,
		headers: req.headers,
	});
};
