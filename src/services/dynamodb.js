import AWS from 'aws-sdk';

AWS.config.update({ region: 'us-east-1' });

export const call = (action, params) => {
	const dynamoDB = new AWS.DynamoDB.DocumentClient();
	return dynamoDB[action](params).promise();
};

export const saveUser = item => {
	const params = {
		TableName: process.env.USERS_TABLE,
		Item: item,
	};
	return call('put', params);
};

export const deleteUser = userId => {
	const params = {
		TableName: process.env.USERS_TABLE,
		Key: { userId },
	};
	return call('delete', params);
};

export const getUser = userId => {
	const params = {
		TableName: process.env.USERS_TABLE,
		Key: { userId },
	};
	return call('get', params);
};

export const queryUser = phoneNumber => {
	const params = {
		TableName: process.env.USERS_TABLE,
		IndexName: 'phoneNumber-index',
		KeyConditionExpression: 'phoneNumber = :phoneNumber',
		ExpressionAttributeValues: { ':phoneNumber': phoneNumber },
	};
	return call('query', params);
};

export const saveDevice = item => {
	const params = {
		TableName: process.env.DEVICES_TABLE,
		Item: item,
	};
	return call('put', params);
};

export const queryDevices = userId => {
	const params = {
		TableName: process.env.DEVICES_TABLE,
		KeyConditionExpression: 'userId = :userId',
		ExpressionAttributeValues: { ':userId': userId },
	};
	return call('query', params);
};

export const saveAccount = item => {
	const params = {
		TableName: process.env.ACCOUNTS_TABLE,
		Item: item,
	};
	return call('put', params);
};

export const queryInvitee = invitee => {
	const params = {
		TableName: process.env.INVITEES_TABLE,
		IndexName: 'invitee-index',
		KeyConditionExpression: 'invitee = :invitee',
		ExpressionAttributeValues: { ':invitee': invitee },
	};
	return call('query', params);
};

export const saveInvitee = inviteeItem => {
	const params = {
		TableName: process.env.INVITEES_TABLE,
		Item: inviteeItem,
	};
	return call('put', params);
};
