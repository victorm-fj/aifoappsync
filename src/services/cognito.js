import AWS from 'aws-sdk';

AWS.config.update({ region: 'us-east-1' });

export const call = (action, params) => {
	const cognitoIdentityService = new AWS.CognitoIdentityServiceProvider();
	return cognitoIdentityService[action](params).promise();
};

export const signUp = params => {
	return call('signUp', params);
};

export const initAuth = (username, password) => {
	const params = {
		AuthFlow: 'USER_PASSWORD_AUTH',
		ClientId: process.env.COGNITO_APP_CLIENT_ID,
		AuthParameters: {
			USERNAME: username,
			PASSWORD: password,
		},
	};
	return call('initiateAuth', params);
};
