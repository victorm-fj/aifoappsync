import plaid from 'plaid';
import util from 'util';

const PLAID_CLIENT_ID = process.env.PLAID_CLIENT_ID;
const PLAID_SECRET = process.env.PLAID_SECRET;
const PLAID_PUBLIC_KEY = process.env.PLAID_PUBLIC_KEY;
const PLAID_ENV = process.env.PLAID_ENV;

export const getAccessToken = async publicToken => {
	const client = new plaid.Client(
		PLAID_CLIENT_ID,
		PLAID_SECRET,
		PLAID_PUBLIC_KEY,
		plaid.environments[PLAID_ENV]
	);
	client.exchangePublicToken = util.promisify(client.exchangePublicToken);
	return await client.exchangePublicToken(publicToken);
};
