type User {
	userId: ID
	firstName: String
	phoneNumber: String
	lastName: String
	dateBirth: String
	email: String
	postCode: String
	address: String
	city: String
	status: String
	devices: [Device!]
	accounts: [Account!]
}

type Device {
	userId: ID
	deviceId: ID
	phoneNumber: String
	name: String
	brand: String
	enabled: Boolean
}

type Account {
	userId: ID
	# itemId: ID | should not be visible to end user
	# accessToken: String | should not be visible to end user
	institution: Institution
	accounts: [BankAccount!]
}

type Institution {
	institutionId: ID
	name: String
}

type BankAccount {
	accountId: ID
	mask: String
	name: String
	subtype: String
	type: String
}

type Invitee {
	userId: ID
	invitee: String
	status: String
	name: String
}

input DeviceInput {
	deviceId: ID!
	phoneNumber: String
	name: String!
	brand: String!
}

type DeviceConnection {
	nextToken: String
	devices: [Device]
}

type InviteeConnection {
	nextToken: String
	invitees: [Invitee]
}

type Query {
	getUser(phoneNumber: String!): User
	getDevices(userId: ID!): DeviceConnection
	getInvitees(userId: ID!): InviteeConnection
}

type Mutation {
	registerUser(
		username: String!
		password: String!
		firstName: String!
		phoneNumber: String!
		device: DeviceInput!
	): User
	updateProfile(userId: ID!, profile: ProfileInput!): User
	trackDevice(userId: ID!, device: DeviceInput!, enabled: Boolean!): Device
	addAccount(
		userId: ID!
		publicToken: String!
		institution: InstitutionInput!
		accounts: [BankAccountInput!]
	): Account
	saveInvitee(userId: ID!, invitee: String!, name: String): Invitee
	updateInvitee(userId: ID!, invitee: String!, status: String!): Invitee
	confirmUser(userId: ID!): User
}

input ProfileInput {
	firstName: String
	lastName: String
	dateBirth: String
	email: String
	postCode: String
	address: String
	city: String
}

input InstitutionInput {
	institutionId: ID
	name: String
}

input BankAccountInput {
	accountId: ID
	mask: String
	name: String
	subtype: String
	type: String
}

type Subscription {
	subscribeToNewDevice(userId: ID!): Device
		@aws_subscribe(mutations: ["trackDevice"])
	subscribeToInviteeStatus(userId: ID!): Invitee
		@aws_subscribe(mutations: ["saveInvitee", "updateInvitee"])
}

schema {
	query: Query
	mutation: Mutation
	subscription: Subscription
}
